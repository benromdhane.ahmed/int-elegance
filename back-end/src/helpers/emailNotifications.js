const nodeMailer = require("nodemailer");

/* Send User Auth */
const sendUserAuth = (firstName, lastName, email, password) => {
  let transporter = nodeMailer.createTransport({
    service: process.env.SERVICE,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  let mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email,
    subject: "Welcome to INT-Elegance",
    text:
      `Bonjour ${firstName} ${lastName},\n\n` +
      "Félicitations ! Votre compte INT-Elegance a été créé.\n" +
      "Voici vos informations de connexion :\n" +
      "Email : " +
      email +
      "\n" +
      "Mot de passe : " +
      password +
      "\n\n" +
      "Cordialement,\n",
  };
  transporter.sendMail(mailOptions);
};

/**Send Meeting Invitation */
const sendInvitation = (
  senderFirstName,
  senderLastName,
  participants,
  meeting
) => {
  let transporter = nodeMailer.createTransport({
    service: process.env.SERVICE,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  for (const participant of participants) {
    let text = "";
    let subject = "";

    if (meeting.end > meeting.start) {
      subject = "Invitation Formation";
      text =
        `Bonjour ${participant.firstName} ${participant.lastName},` +
        "\n" +
        `${senderFirstName} ${senderLastName} vous a invité à la formation ${meeting.title} qui va se dérouler dans la période allant de ${meeting.start} à ${meeting.end} de ${meeting.startTime} à ${meeting.endTime} à la salle ${meeting.room}.`;
    } else {
      subject = "Invitation Réunion";
      text =
        `Bonjour ${participant.firstName} ${participant.lastName},` +
        "\n" +
        `${senderFirstName} ${senderLastName} vous a invité à la réunion ${meeting.title} qui va se dérouler le ${meeting.start} de ${meeting.startTime} à ${meeting.endTime} à la salle ${meeting.room}.`;
    }

    let mailOptions = {
      from: process.env.EMAIL_ADDRESS,
      to: participant.email,
      subject: subject,
      text: text,
    };
    transporter.sendMail(mailOptions);
  }
};

/**Send Canceled Meeting Notification */
const SendCanceledMeeting = (
  senderFirstName,
  senderLastName,
  participants,
  meeting
) => {
  let transporter = nodeMailer.createTransport({
    service: process.env.SERVICE,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  for (const participant of participants) {
    let text = "";
    let subject = "";

    if (meeting.end > meeting.start) {
      subject = "Annulation Formation";
      text =
        `Bonjour ${participant.firstName} ${participant.lastName},` +
        "\n" +
        `La formation ${meeting.title} organisé par ${senderFirstName} ${senderLastName} prévu de ${meeting.start} à ${meeting.end} est annulé.`;
    } else {
      subject = "Annulation Réunion";
      text =
        `Bonjour ${participant.firstName} ${participant.lastName},` +
        "\n" +
        `La réunion ${meeting.title} organisé par ${senderFirstName} ${senderLastName} prévu le ${meeting.start} de ${meeting.startTime} à ${meeting.endTime} est annulé.`;
    }

    let mailOptions = {
      from: process.env.EMAIL_ADDRESS,
      to: participant.email,
      subject: subject,
      text: text,
    };
    transporter.sendMail(mailOptions);
  }
};

module.exports = {
  sendUserAuth,
  sendInvitation,
  SendCanceledMeeting,
};
