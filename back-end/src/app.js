var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require("cors");
const sequelize = require('./config/sequelize')
const dotenv = require("dotenv");

dotenv.config();
const cron = require('node-cron');
const db = require("../src/models/index");
const notificationModel = db.notification
const projectModel = db.project;
const phaseModel = db.phase;
const { Op } = require("sequelize");
const consts = require('./config/const')
const http = require("http");
const { Server } = require("socket.io");
const jwt = require('jsonwebtoken')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var rolesRouter = require('./routes/roles');
var permissionsRouter = require('./routes/permissions');
var authRouter = require('./routes/auth');
var projectRouter= require('./routes/project')
var phaseRouter= require('./routes/phase')
var taskRouter= require('./routes/task')
var trailRouter= require('./routes/trail')
var meetingRouter= require('./routes/meeting')
var roomRouter= require('./routes/room')
var fileRouter = require('./routes/files')
var messageRouter = require('./routes/message')
var notificationRouter = require('./routes/notification')
var app = express();

const httpServer = http.createServer(app);
const io = new Server(httpServer, {
  cors: { origin: ["http://localhost:4200", "http://localhost:8081","http://localhost:3000"] },
});
global.__basedir = path.join(__dirname, ".."); 
const conversations = new Map();
const users = []; // Store connected users here

function getUsersList() {
  const connectedUsers = Array.from(io.of("/").sockets)
    .filter(([id, socket]) => socket.user && socket.user.id)
    .map(([id, socket]) => ({
      id: socket.user.id,
      socketIds: [id],
    }));

  return connectedUsers
  
}
// const JWT_SECRET= 'accessSecret'
io.use(async (socket, next) => {
  const token = socket.handshake.auth.token;
 
  
  try {
    const user = await jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    socket.user = user.user;
    const usersList = getUsersList();
    // socket.broadcast.emit("user-connected", socket.user.id);
    socket.emit("users", users);
    socket.broadcast.emit("user-connected", socket.user.id)
    next();
  } catch (e) {
    console.log("Authentication error:", e.message);
    return next(new Error("Authentication error"));
  }
});

io.on("connection", async (socket) => {
  console.log("a user connected");
  
  const userIndex = users.findIndex((connectedUser) => connectedUser.id === socket.user.id);
  if (userIndex === -1) {
    users.push({ userid: socket.user.id, socketIds: [socket.id] });
  } else {
    users[userIndex].socketIds.push(socket.id);
  }
  console.log(users)
  
  
  socket.on('notif', (data)=>{
    console.log('xxx',data)
  })

  socket.on('join-conversation', ({ conversationId, socketId }) => {
  if (conversations.has(conversationId)) {
    const socketIds = conversations.get(conversationId);
    if (!socketIds.includes(socketId)) {
      socketIds.push(socketId);
    }
  } else {
    conversations.set(conversationId, [socketId]);
  }

  console.log(conversations);
});

  socket.on('new-message', async({ conversationId, message, destinationId }) => {
    console.log(message)
    const notification = {
      from:`${socket.user.firstName} ${socket.user.lastName}`,
      subject:`${socket.user.firstName} has sent you a message`,
      useravatar:`${socket.user.image}`,
      treated: false,
      to:destinationId
    }
    const currentDateTime= new Date()
    await notificationModel.create(notification).then((lastCreatedNotification) => {
      const id = lastCreatedNotification.id
      socket.broadcast.emit('notif', {notification, currentDateTime,id})
    })

    
  if (conversations.has(conversationId)) {
    const socketIds = conversations.get(conversationId);
    socketIds.forEach((socketId) => {
      io.to(socketId).emit('send-message', message);
      
    });
  }
});
socket.on('user-typing', ({ conversationId, isTyping, userId}) => {
  console.log(conversationId)
  console.log(isTyping)
  console.log(userId)
  console.log(socket.user.id)
 // socket.to(conversationId).emit('typing-status', { conversationId, isTyping });
  if (conversations.has(conversationId)) {
    const socketIds = conversations.get(conversationId);
    socketIds.forEach((socketId) => {
      io.to(socketId).emit('typing-status', { conversationId, isTyping , userId});
    });
  }
});

io.on('notification', (data)=>{
  console.log(data)

})

socket.on("disconnect", () => {
  console.log("user disconnected");
  const userId = socket.user.id;

  const userIndex = users.findIndex((connectedUser) => connectedUser.userid === userId);
  if (userIndex !== -1) {
    const socketIds = users[userIndex].socketIds;
    const index = socketIds.indexOf(socket.id);
    if (index !== -1) {
      socketIds.splice(index, 1);
      if (socketIds.length === 0) {
    
        users.splice(userIndex, 1);
      }
    }
  }
  console.log(users);
  io.emit("users", users);

  io.emit("user-disconnected", socket.user.id);
});
});


app.use(
  cors({
    origin: ["http://localhost:4200","http://localhost:8081"],
    credentials: true,
  })
)

// Security configuration
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    " Content-Type"
  );

  next();
});
//body parser
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static('public'));

sequelize
  .authenticate()
  .then(() => {
    console.log("connected..");
  })
  .catch((err) => {
    console.log("Error" + err);
  });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/role', rolesRouter);
app.use('/permission',permissionsRouter)
app.use('/auth', authRouter)
app.use('/project', projectRouter)
app.use('/phase', phaseRouter)
app.use('/task', taskRouter)
app.use('/trail', trailRouter)
app.use('/meeting', meetingRouter)
app.use('/room', roomRouter)
app.use("/files", fileRouter)
app.use('/message', messageRouter)
app.use('/notification', notificationRouter)
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const currentMinute = new Date().getMinutes();
const scheduledMinute = (currentMinute + 59) % 60;

cron.schedule(`${scheduledMinute} * * * *`, async () => {
  try {
    const today = new Date();
    const projects = await projectModel.findAll({
      include: {
        model: phaseModel,
        as: 'phases',
        where: {
          [Op.and]: [
            { startdate: { [Op.not]: null } }, 
            { enddate: { [Op.not]: null } }   
          ]
        }
      },
    });
    for (const project of projects) {
      for (const phase of project.phases) {
        if (new Date(phase.startDate).toDateString() === today.toDateString()) {
          phase.status = consts.taskStatus.inProgress;
          await phase.save();
        } 
        const prevPhaseIndex = project.phases.indexOf(phase) - 1;
        if (prevPhaseIndex >= 0) {
          const prevPhase = project.phases[prevPhaseIndex];
          const prevPhaseEndDate = new Date(prevPhase.endDate);
          const yesterday = new Date(today);
          yesterday.setDate(yesterday.getDate() - 1);
          if (prevPhase.status === consts.taskStatus.inProgress && prevPhaseEndDate.toDateString() === yesterday.toDateString()) {
            prevPhase.status = consts.taskStatus.completed;
            await prevPhase.save();
          }
        }
      }
    }
  } catch (error) {
    console.error('Scheduled task error:', error);
  }
});

module.exports = { app, httpServer};
